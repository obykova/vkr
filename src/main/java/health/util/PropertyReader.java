package health.util;

import java.util.Properties;

public interface PropertyReader {

    Properties getProperties();
}
