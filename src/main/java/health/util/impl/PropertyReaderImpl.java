package health.util.impl;

import health.util.PropertyReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

@Component
public class PropertyReaderImpl implements PropertyReader {

    private static final Logger log = LogManager.getLogger(PropertyReaderImpl.class);
    private static final String APPLICATION_PROPERTIES = "application.properties";
    private static final String EMPTY_STRING = "";

    public Properties getProperties() {
        Properties properties = new Properties();
        try {
            String property = Objects.requireNonNull(Thread.currentThread()
                    .getContextClassLoader().getResource(EMPTY_STRING)).getPath()
                    + APPLICATION_PROPERTIES;
            properties = new Properties();
            properties.load(new FileInputStream(property));
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return properties;
    }
}
