package health.util.impl;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class SessFactory {

    private static SessionFactory instance;

    private SessFactory() {
    }

    public static org.hibernate.SessionFactory getInstance() {
        if (instance == null) {
            StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
            instance = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        }
        return instance;
    }
}