package health.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/resetPassword")
public class ResetPasswordController {

    @GetMapping
    public String resetPasswordGet(ModelMap model) {
        return "reset";
    }

    @PostMapping
    public String resetPasswordPost(ModelMap model) {
        return "reset";
    }
}
