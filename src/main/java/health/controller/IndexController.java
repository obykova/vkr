package health.controller;

import health.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/")
public class IndexController {

    private UserService userService;

    private static final Logger log = LogManager.getLogger(IndexController.class);

    public IndexController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String indexGet(Principal principal, ModelMap model) {
        if (principal != null) {
            String username = userService.getUserName(principal);
            log.info("username: " + username);
            model.addAttribute("username", username);
        }
        return "index";
    }

    @PostMapping
    public String indexPost(Principal principal, ModelMap model) {
        if (principal != null) {
            String username = userService.getUserName(principal);
            log.info("username: " + username);
            model.addAttribute("username", username);
        }
        return "index";
    }

}
