package health.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/sleep")
public class SleepController {

    @GetMapping
    public String sleepGet(Principal principal, ModelMap model) {
        return "sleep";
    }

    @PostMapping
    public String sleepPost(Principal principal, ModelMap model) {
        return "sleep";
    }
}