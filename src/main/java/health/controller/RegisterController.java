package health.controller;

import health.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Date;

@Controller
@RequestMapping("/register")
public class RegisterController {

    private UserService userService;

    private static final Logger log = LogManager.getLogger(RegisterController.class);

    public RegisterController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String registerGet(ModelMap modelMap) {
        modelMap.addAttribute("hasError", false);
        return "register";
    }

    @PostMapping
    public String registerPost(@RequestParam String login, @RequestParam String password,
                               @RequestParam String question, @RequestParam String secretWord,
                               @RequestParam String name, @RequestParam int height,
                               @RequestParam int weight, @RequestParam Date birthDate,
                               @RequestParam String gender, ModelMap modelMap) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        password = passwordEncoder.encode(password);
        boolean res = userService.addUserAllParams(login, password, question, secretWord, name, height, weight, birthDate, gender, "U");
        log.info("result: " + res);
        if (res) {
            return "index";
        }
        modelMap.addAttribute("hasError", true);
        return "register";
    }
}
