package health.controller;

import health.service.ImbHistoryService;
import health.service.UserService;
import health.service.impl.CalculatorImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

@Controller
public class CalculatorController {

    private UserService userService;
    private CalculatorImpl calculator;
    private ImbHistoryService imbHistoryService;

    private static final Logger log = LogManager.getLogger(CalculatorController.class);

    public CalculatorController(UserService userService, CalculatorImpl calculator, ImbHistoryService imbHistoryService) {
        this.userService = userService;
        this.calculator = calculator;
        this.imbHistoryService = imbHistoryService;
    }

    @GetMapping("calculator")
    public String calculatorGet(Principal principal, ModelMap model) {
        log.info("get calculator");
        if (principal != null) {
            String username = userService.getUserName(principal);
            log.info("username: " + username);
            model.addAttribute("username", username);
        }
        return "calculator";
    }

    @GetMapping("calculator/history")
    public String calculatorHistoryGet(Principal principal, ModelMap model) {
        //todo
        log.info("get calculator history");
        if (principal != null) {
            String username = userService.getUserName(principal);
            log.info("username: " + username);
            model.addAttribute("username", username);
            model.addAttribute("history", imbHistoryService.getByUsername(username));
            log.info("model: " + model);
        }
        return "history";
    }

    @PostMapping("calculator")
    public String calculatorPost(Principal principal, ModelMap model,
                                 @RequestParam double height, @RequestParam double weight) {
        log.info("post calculator, weight " + weight + ", height " + height);
        if (principal != null) {
            String username = userService.getUserName(principal);
            log.info("username: " + username);
            model.addAttribute("username", username);
            if (height != 0 && weight != 0) {
                imbHistoryService.add(username, Math.round(calculator.calculateIMT(weight, height)));
            }
        }
        if (height != 0 && weight != 0) {
            CalculatorImpl.Weight weightObject = calculator.getIMTResult(calculator.calculateIMT(weight, height));
            log.info("description: " + weightObject.getDescription());
            log.info("recommendations: " + weightObject.getRecommendation());
            model.addAttribute("description", weightObject.getDescription());
            model.addAttribute("recommendations", weightObject.getRecommendation());
        }
        return "calculator";
    }
}
