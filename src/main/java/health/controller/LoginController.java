package health.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
public class LoginController {

    private static final Logger log = LogManager.getLogger(LoginController.class);

    @GetMapping("/login")
    public String loginGet(ModelMap model) {
        log.info("login get");
        model.addAttribute("hasError", false);
        return "login";
    }

    @PostMapping("/login")
    public String loginPost(ModelMap model) {
        log.info("login post");
        model.addAttribute("hasError", true);
        return "login";
    }

    @GetMapping("/login-error")
    public String loginErrorGet(ModelMap model) {
        log.info("login error get");
        model.addAttribute("hasError", true);
        return "login";
    }
}