package health.controller;

import health.model.User;
import health.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.Date;

@Controller
@RequestMapping("/my_page/change_info")
public class ChangeUserInfoController {

    private static final double MILLISECONDS_TO_DAYS_VALUE = 31104000000L;

    private UserService userService;

    private static final Logger log = LogManager.getLogger(ChangeUserInfoController.class);


    public ChangeUserInfoController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String changeUserInfoGet(Principal principal, ModelMap model) {
        String username = userService.getUserName(principal);
        log.info("username: " + username);
        User user = userService.getUser(username);
        double userAge = Math.floor((new Date().getTime() - user.getBirthday().getTime()) / MILLISECONDS_TO_DAYS_VALUE);
        model.addAttribute("username", username);
        model.addAttribute("birthday", user.getBirthday());
        model.addAttribute("userAge", userAge);
        model.addAttribute("gender", user.getGender());
        model.addAttribute("height", user.getHeight());
        model.addAttribute("weight", user.getWeight());
        model.addAttribute("login", user.getLogin());
        model.addAttribute("password", user.getPassword());
        log.info("model: " + model);

        return "changeInfo";
    }

    @PostMapping
    public String changeUserInfoPost(Principal principal, ModelMap model,
                                     @RequestParam String name,
//                                     @RequestParam String password,
                                     @RequestParam Double weight, @RequestParam Double height,
//                                     @RequestParam Date birthday,
                                     @RequestParam String gender) {

        log.info("name " + name + " weight "+ weight + " height " +height + " birthday "
                + " gender " + gender);
        String username = userService.getUserName(principal);
        log.info("username: " + username);
        User user = userService.getUser(username);

        User newUser = new User();
        user.setName(name);
        user.setWeight(weight);
        user.setHeight(height);
//        user.setBirthday(birthday);
        user.setGender(gender);

        log.info("new user " + newUser.toString());
        userService.update(user, newUser);
        return "changeInfo";
    }
}
