package health.controller;

import health.model.User;
import health.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.Date;


@Controller
@RequestMapping("/my_page")
public class MyPageController {

    private static final double MILLISECONDS_TO_DAYS_VALUE = 31104000000L;

    private UserService userService;

    private static final Logger log = LogManager.getLogger(MyPageController.class);

    public MyPageController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String myPageGet(Principal principal, ModelMap model) {
        String username = userService.getUserName(principal);
        log.info("username: " + username);
        User user = userService.getUser(username);
        setAllParams(model, user);
        log.info("model: " + model);

        return "my_page";
    }

    @PostMapping
    public String myPagePost(Principal principal, ModelMap model) {
        String username = userService.getUserName(principal);
        log.info("username: " + username);
        User user = userService.getUser(username);
        setAllParams(model, user);
        log.info("model: " + model);

        return "my_page";
    }

    private void setAllParams(ModelMap model, User user) {
        double userAge = Math.floor((new Date().getTime() - user.getBirthday().getTime()) / MILLISECONDS_TO_DAYS_VALUE);
        log.info("userAge: " + userAge);

        model.addAttribute("username", user.getName());
        model.addAttribute("age", (int) userAge);
        model.addAttribute("gender", user.getFormatGender());
        model.addAttribute("height", user.getHeight());
        model.addAttribute("weight", user.getWeight());
        model.addAttribute("login", user.getLogin());
        model.addAttribute("password", user.getPassword());
    }

/*    @PostMapping
    public String shop(Principal principal, @RequestParam(required = false) String purchase,
                       ModelMap model) {

        String username = userService.getUsername(principal);
        log.info("username: " + username + ", purchase: " + purchase);
        model.addAttribute("username", username);

        boolean result = assortmentService.addUserPurchases(username, purchase);
        log.info("add to db purchase: " + result);

        model.addAttribute("userPurchases", assortmentService.getUserPurchases(username));
        model.addAttribute("allAssortment", assortmentService.getAll());
        log.info("model: " + model);

        return "Shop";
    }*/
}
