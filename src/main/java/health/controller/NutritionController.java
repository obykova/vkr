package health.controller;

import health.model.Nutrition;
import health.model.Product;
import health.model.User;
import health.service.NutritionService;
import health.service.ProductService;
import health.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class NutritionController {
    private static final Logger log = LogManager.getLogger(NutritionController.class);

    private UserService userService;
    private ProductService productService;
    private NutritionService nutritionService;

    public NutritionController(UserService userService, ProductService productService, NutritionService nutritionService) {
        this.userService = userService;
        this.productService = productService;
        this.nutritionService = nutritionService;
    }

    @GetMapping("nutrition")
    public String nutritionGet(Principal principal, ModelMap model, @RequestParam(required = false) String dayTime) {
        log.info("nutrition get");
        String username = userService.getUserName(principal);
        log.info("username: " + username);
        model.addAttribute("username", username);

        if (dayTime != null) {
            model.addAttribute("chosenTime", dayTime);
        } else {
            User user = userService.getUser(username);
            setNutritions(model, user, "morning");
            model.addAttribute("chosenTime", "morning");
        }

        log.info("model: " + model);
        return "nutrition";
    }

    @GetMapping("nutrition/{type}")
    public String nutritionTypeGet(Principal principal, ModelMap model, @PathVariable("type") String type) {
        log.info("nutrition get. type: " + type);
        String username = userService.getUserName(principal);
        log.info("username: " + username);
        model.addAttribute("username", username);
        model.addAttribute("products", productService.getByType(type));

        log.info("model: " + model);
        return "nutritionType";
    }

    @GetMapping("nutrition/product/{id}")
    public String nutritionIdGet(Principal principal, ModelMap model, @PathVariable("id") int id) {
        log.info("nutrition get. id: " + id);
        String username = userService.getUserName(principal);
        log.info("username: " + username);
        model.addAttribute("username", username);
        model.addAttribute("productId", id);

        log.info("model: " + model);
        return "product";
    }

    @PostMapping("nutrition")
    public String nutritionPost(Principal principal, ModelMap model,
                                @RequestParam(required = false) Integer productId,
                                @RequestParam(required = false) Integer count,
                                @RequestParam(required = false) String dayTime,
                                @RequestParam(required = false) Integer deleteId) {
        log.info("nutrition post. productId: " + productId + ", count: " + count + ", daytime: " + dayTime);
        String username = userService.getUserName(principal);
        log.info("username: " + username);
        model.addAttribute("username", username);
        User user = userService.getUser(username);

        if (productId != null) {
            Optional<Product> product = productService.getById(productId);
            product.ifPresent(value -> model.addAttribute("product", value));
            nutritionService.add(user.getId(), productId, count, dayTime);
        }

        if (deleteId != null) {
            log.info("for delete id = " + deleteId);
            nutritionService.delete(deleteId);
            log.info("model chosen time = " + model.getAttribute("chosenTime"));
            log.info("day time = " + dayTime);
        }

        if (dayTime != null) {
            model.addAttribute("chosenTime", dayTime);
        } else {
            model.addAttribute("chosenTime", "morning");
        }

        setNutritions(model, user, dayTime);

        return "nutrition";
    }

    private void setNutritions(ModelMap model, User user, String dayTime) {
        List<Nutrition> nutritions = nutritionService.getByUserAndTime(user.getId(), dayTime);
        List<Product> products = new ArrayList<>();
        for (Nutrition nutrition : nutritions) {
            products.add(productService.getById(nutrition.getProductId()).get());
        }
        model.addAttribute("nutritions", nutritions);
        model.addAttribute("products", products);
        log.info("products: " + model.getAttribute("products"));
    }
}
