package health.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/water")
public class WaterController {

    @GetMapping
    public String waterGet(Principal principal, ModelMap model) {
        return "water";
    }

    @PostMapping
    public String waterPost(Principal principal, ModelMap model) {
        return "water";
    }
}