package health.service;

import health.service.impl.CalculatorImpl;

public interface Calculator {

    double calculateIMT(double weight, double height);

    CalculatorImpl.Weight getIMTResult(double imt);
}
