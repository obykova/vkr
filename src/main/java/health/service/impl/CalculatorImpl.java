package health.service.impl;

import health.service.Calculator;
import org.springframework.stereotype.Service;

@Service
public class CalculatorImpl implements Calculator {

    @Override
    public double calculateIMT(double weight, double height) {
        return weight / Math.pow(height, 2) * 10000;
    }

    @Override
    public Weight getIMTResult(double imt) {
        return Weight.search(imt);
    }
    //todo добавить описание рекомендаций
    public enum Weight {
        EXTREME_UNDERWEIGHT(16.5, "Крайний недостаток веса", "Рекомендации..."),
        UNDERWEIGHT(18.4, "Недостаток в весе", "Рекомендуется набрать "),
        NORMAL(24.9, "Нормальный вес тела", "Сейчас ваш рост и вес находятся в оптимальных значениях. Продолжайте в том же духе!"),
        OVERWEIGHT(30, "Избыточная масса тела", "Рекомендации..."),
        OVERWEIGHT_FIRST(34.9, "Ожирение (Класс I)", "Рекомендации..."),
        OVERWEIGHT_SECOND(40, "Ожирение (Класс II - тяжелое)", "Рекомендации..."),
        OVERWEIGHT_THIRD(Double.POSITIVE_INFINITY, "Ожирение (Класс III - крайне тяжелое)", "Рекомендации...");

        private double imt;
        private String description;
        private String recommendation;

        public double getImt() {
            return imt;
        }

        public String getDescription() {
            return description;
        }

        public String getRecommendation() {
            return recommendation;
        }

        Weight(double imt, String description, String recommendation) {
            this.imt = imt;
            this.description = description;
            this.recommendation = recommendation;
        }

        static Weight search(double imt) {
            for (Weight value : Weight.values()) {
                if (value.imt > imt) {
                    return value;
                }
            }
            return OVERWEIGHT_THIRD;
        }
    }
}
