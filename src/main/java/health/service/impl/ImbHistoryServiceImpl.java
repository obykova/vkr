package health.service.impl;

import health.model.ImbHistory;
import health.model.User;
import health.repository.ImbHistoryRepository;
import health.repository.UserRepository;
import health.service.ImbHistoryService;
import health.util.impl.ListUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ImbHistoryServiceImpl implements ImbHistoryService {

    private static final Logger log = LogManager.getLogger(ImbHistoryServiceImpl.class);

    private ImbHistoryRepository imbHistoryRepository;
    private UserRepository userRepository;
    private ListUtil listUtil;

    public ImbHistoryServiceImpl(ImbHistoryRepository imbHistoryRepository, UserRepository userRepository, ListUtil listUtil) {
        this.imbHistoryRepository = imbHistoryRepository;
        this.userRepository = userRepository;
        this.listUtil = listUtil;
    }

    @Override
    public boolean add(String username, double value) {
        Date date = new Date();
        log.info("date: " + date);
        ImbHistory imbHistory = new ImbHistory();
        Optional<User> user = userRepository.getByName(username);
        imbHistory.setValue(value);
        imbHistory.setDate(date);
        imbHistory.setUserId(user.get().getId());
        log.info("imbHistory: " + imbHistory.toString());

        imbHistoryRepository.add(imbHistory);
        return false;
    }

    @Override
    public List<ImbHistory> getByUsername(String username) {
        return listUtil.reverseList(imbHistoryRepository.getByUsername(username));
    }

}
