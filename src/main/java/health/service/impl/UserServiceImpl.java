package health.service.impl;

import health.model.Nutrition;
import health.model.User;
import health.repository.UserRepository;
import health.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private static final Logger log = LogManager.getLogger(UserServiceImpl.class);

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public String getUserName(Principal principal) {
        return principal.getName();
    }

    @Override
    public User getUser(String login) {
        Optional<User> user = userRepository.getByName(login);
        return user.orElse(null);
    }

    @Override
    public boolean addUserAllParams(String login, String password, String question, String word,
                                    String name, double height, double weight, Date birthday,
                                    String gender, String role) {
        LocalDateTime now = LocalDateTime.now();
        Date dateNow = Date.valueOf(now.toLocalDate());
        User user = new User(login, password, question, word, name, height, weight, birthday, gender, dateNow, role);
        log.info(user.toString());
        if (userRepository.getByName(login).isPresent()) {
            log.info("Человек с таким логином ( " + login + " ) существует");
            return false;
        }
        return userRepository.add(user);
    }

    @Override
    public List<Nutrition> getTodayNutrition(String username) {
        return null;
    }

    @Override
    public boolean update(User user, User newUser) {
        userRepository.update(user, newUser);
        return false;
    }
}
