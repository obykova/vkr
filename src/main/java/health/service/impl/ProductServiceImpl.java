package health.service.impl;

import health.model.Product;
import health.repository.ProductRepository;
import health.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Optional<Product> getById(long id) {
        return productRepository.getById(id);
    }

    @Override
    public Optional<Product> getByName(String name) {
        return productRepository.getByName(name);
    }

    @Override
    public List<Product> getByType(String type) {
        return productRepository.getByType(type);
    }

}
