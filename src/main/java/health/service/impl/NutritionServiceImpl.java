package health.service.impl;

import health.model.Nutrition;
import health.repository.NutritionRepository;
import health.service.NutritionService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class NutritionServiceImpl implements NutritionService {

    private NutritionRepository nutritionRepository;

    public NutritionServiceImpl(NutritionRepository nutritionRepository) {
        this.nutritionRepository = nutritionRepository;
    }

    @Override
    public List<Nutrition> getBuUserId(long id) {
        return null;
    }

    @Override
    public List<Nutrition> getByUserDate(long id, Date date) {
        return null;
    }

    @Override
    public List<Nutrition> getByUserAndTime(long id, String dayTime) {
        Nutrition nutrition = new Nutrition();
        nutrition.setUserId(id);
        nutrition.setDate(new Date());
        nutrition.setTime(dayTime);

        return nutritionRepository.getByUserAndTime(nutrition);
    }

    @Override
    public boolean add(long userId, long productId, int count, String dayTime) {
        Nutrition nutrition = new Nutrition();
        nutrition.setUserId(userId);
        nutrition.setProductId(productId);
        nutrition.setDate(new Date());
        nutrition.setTime(dayTime);
        nutrition.setMassa(count);

        return nutritionRepository.add(nutrition);
    }

    @Override
    public boolean delete(long id) {
        nutritionRepository.delete(id);
        return false;
    }
}
