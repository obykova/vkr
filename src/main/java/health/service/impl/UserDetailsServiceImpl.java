package health.service.impl;

import health.model.User;
import health.repository.UserRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

//    @Override
//    public UserDetails loadUserByUsername(String name) {
//        User user = userRepository
//                .getByName(name)
//                .orElseThrow(() -> new UsernameNotFoundException(String.format("User %s not found", name)));
//
//        List<GrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"));
//        return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), authorities);
//    }

//    public UserDetails loadUserByUsername(final String name) {
//        User user = userRepository
//                .getByName(name)
//                .orElseThrow(() -> new UsernameNotFoundException(String.format("User %s not found", name)));
//
//        Set<GrantedAuthority> roles = new HashSet();
//        roles.add(new SimpleGrantedAuthority("ROLE_USER"));
//        if (user.getName().equals("admin")) {
//            roles.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
//        }
//        return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), roles);
//    }
        @Override
        public UserDetails loadUserByUsername(final String name) {
            User user = userRepository.getByName(name)
                    .orElseThrow(() -> new UsernameNotFoundException(String.format("User %s not found", name)));
            Set<GrantedAuthority> roles = new HashSet();
            roles.add(new SimpleGrantedAuthority("ROLE_USER"));
            if (user.getRole().equals("A")){
                roles.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
            }
            return new org.springframework.security.core.userdetails.User(user.getLogin(),
                    user.getPassword(), roles);
        }
}
