package health.service;

import health.model.ImbHistory;

import java.util.List;

public interface ImbHistoryService {

    boolean add(String username, double value);

    List<ImbHistory> getByUsername(String username);
}
