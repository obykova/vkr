package health.service;

import health.model.Nutrition;

import java.util.Date;
import java.util.List;

public interface NutritionService {

    List<Nutrition> getBuUserId(long id);

    List<Nutrition> getByUserDate(long id, Date date);

    List<Nutrition> getByUserAndTime(long id, String time);

    boolean add(long userId, long productId, int count, String dayTime);

    boolean delete(long id);

}
