package health.service;

import health.model.Nutrition;
import health.model.User;

import java.security.Principal;
import java.sql.Date;
import java.util.List;

public interface UserService {

    String getUserName(Principal principal);

    User getUser(String login);

    boolean addUserAllParams(String login, String password, String question, String word,
                             String name, double height, double weight, Date birthday,
                             String gender, String role);

    List<Nutrition> getTodayNutrition(String username);

    boolean update(User user, User newUser);
}
