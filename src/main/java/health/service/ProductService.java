package health.service;

import health.model.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    Optional<Product> getById(long id);

    Optional<Product> getByName(String name);

    List<Product> getByType(String type);

}
