package health.repository;

import health.model.Product;

import java.util.List;
import java.util.Optional;

public interface ProductRepository {

    Optional<Product> getById(long id);

    Optional<Product> getByName(String name);

    List<Product> getByType(String type);
}
