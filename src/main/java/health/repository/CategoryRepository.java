package health.repository;

import health.model.Category;

import java.util.Optional;

public interface CategoryRepository {

    Optional<Category> getById(long id);

    Optional<Category> getByName(String name);
}
