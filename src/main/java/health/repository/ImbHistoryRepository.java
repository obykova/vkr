package health.repository;

import health.model.ImbHistory;

import java.util.List;

public interface ImbHistoryRepository {

    boolean add(ImbHistory imbHistory);

    boolean delete(ImbHistory imbHistory);

    List<ImbHistory> getByUsername(String username);

}
