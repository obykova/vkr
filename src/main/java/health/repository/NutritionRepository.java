package health.repository;

import health.model.Nutrition;

import java.util.Date;
import java.util.List;

public interface NutritionRepository {

    List<Nutrition> getBuUserId(long id);

    List<Nutrition> getByUserDate(long id, Date date);

    List<Nutrition> getByUserAndTime(Nutrition nutrition);

    boolean add(Nutrition nutrition);

    boolean delete(long id);
}
