package health.repository.impl;

import health.model.User;
import health.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private static final Logger log = LogManager.getLogger(UserRepositoryImpl.class);

    private static final String URL = "jdbc:h2:~/health";
    private static final String USER = "sa";
    private static final String PASSWORD = "";

    @Override
    public Optional<User> getByName(String username) {
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement st = conn.prepareStatement("SELECT * FROM USERS WHERE LOGIN = ?;")) {
            st.setString(1, username);
            return getUser(st);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return Optional.empty();
    }

    @Override
    public boolean add(User user) {
        boolean result;
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement st = conn.prepareStatement(
                     "INSERT INTO USERS (LOGIN, PASSWORD, QUESTION, SECRET_WORD, NAME, HEIGHT, WEIGHT, REGISTRATION_DATE, BIRTHDAY, GENDER, ROLE) " +
                             "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);")) {
            st.setString(1, user.getLogin());
            st.setString(2, user.getPassword());
            st.setString(3, user.getQuestion());
            st.setString(4, user.getWord());
            st.setString(5, user.getName());
            st.setDouble(6, user.getHeight());
            st.setDouble(7, user.getWeight());
            st.setDate(8, (Date) user.getDateRegister());
            st.setDate(9, (Date) user.getBirthday());
            st.setString(10, user.getGender());
            st.setString(11, "U");
            int rows = st.executeUpdate();
            result = rows != 0;
            log.info("Persisted: " + result + " user: " + user.toString());
        } catch (SQLException e) {
            log.error(e.getMessage());
            return false;
        }
        return result;
    }

    @Override
    public boolean delete(User user) {
        return false;
    }

    @Override
    public Optional<User> getById(Long userId) {
        User user = null;
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement st = conn.prepareStatement(
                     "SELECT * FROM USERS WHERE ID = ?;")) {
            st.setLong(1, userId);
            log.info("Found user by id: " + user);
            user = getUser(st).get();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return Optional.ofNullable(user);
    }

    @Override
    public boolean update(User user, User newUser) {
        boolean result;
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement st = conn.prepareStatement(
                     "UPDATE USERS SET NAME = ?, HEIGHT = ?, WEIGHT = ?, GENDER = ? WHERE ID = ?;")) {
            st.setString(1, newUser.getName());
            st.setDouble(2, newUser.getHeight());
            st.setDouble(3, newUser.getWeight());
//            st.setDate(4, (java.sql.Date) newUser.getBirthday());
            st.setString(4, newUser.getGender());
            st.setLong(5, user.getId());
            int rows = st.executeUpdate();
            result = rows != 0;
            log.info("Updated: " + result + ", new user : " + newUser.toString());
        } catch (SQLException e) {
            log.error(e.getMessage());
            return false;
        }
        return result;
    }

    private Optional<User> getUser(PreparedStatement preparedStatement) {
        User user = null;
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                user = new User();
                user.setId(rs.getLong("id"));
                user.setLogin(rs.getString("login"));

                user.setQuestion(rs.getString("question"));
                user.setWord(rs.getString("secret_word"));
                user.setPassword(rs.getString("password"));
                user.setName(rs.getString("name"));

                user.setHeight(rs.getDouble("height"));
                user.setWeight(rs.getDouble("weight"));

                user.setBirthday(rs.getDate("birthday"));
                user.setGender(rs.getString("gender"));
                user.setDateRegister(rs.getDate("registration_date"));
                user.setRole(rs.getString("role"));
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return Optional.ofNullable(user);
    }
}
