package health.repository.impl;

import health.model.Nutrition;
import health.repository.NutritionRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class NutritionRepositoryImpl implements NutritionRepository {

    private static final Logger log = LogManager.getLogger(NutritionRepositoryImpl.class);

    private static final String URL = "jdbc:h2:~/health";
    private static final String USER = "sa";
    private static final String PASSWORD = "";

    @Override
    public List<Nutrition> getBuUserId(long id) {
        return null;
    }

    @Override
    public List<Nutrition> getByUserDate(long id, Date date) {
        return null;
    }

    @Override
    public List<Nutrition> getByUserAndTime(Nutrition nutrition) {
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement st = conn.prepareStatement(
                     "SELECT * FROM NUTRITION WHERE USER_ID = ? AND TIME = ? AND DATE = ?")) {
            st.setLong(1, nutrition.getUserId());
            st.setString(2, nutrition.getTime());
            st.setDate(3, new java.sql.Date(nutrition.getDate().getTime()));
            List<Nutrition> nutritions = new ArrayList<>();
            Nutrition newNutrition;
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    newNutrition = new Nutrition();
                    newNutrition.setId(rs.getLong("id"));
                    newNutrition.setUserId(rs.getLong("user_id"));
                    newNutrition.setProductId(rs.getLong("product_id"));
                    newNutrition.setDate(rs.getDate("date"));
                    newNutrition.setTime(rs.getString("time"));
                    newNutrition.setMassa(rs.getDouble("massa"));

                    nutritions.add(newNutrition);
                }
            } catch (SQLException e) {
                log.error(e.getMessage());
            }
            return nutritions;
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return new ArrayList<>();
    }

    @Override
    public boolean add(Nutrition nutrition) {
        boolean result;
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement st = conn.prepareStatement(
                     "INSERT INTO NUTRITION (USER_ID, PRODUCT_ID, DATE, TIME, MASSA) VALUES (?, ?, ?, ?, ?);")) {
            st.setLong(1, nutrition.getUserId());
            st.setLong(2, nutrition.getProductId());
            st.setDate(3, new java.sql.Date(nutrition.getDate().getTime()));
            st.setString(4, nutrition.getTime());
            st.setDouble(5, nutrition.getMassa());
            int rows = st.executeUpdate();
            result = rows != 0;
            log.info("Persisted: " + result + " nutrition: " + nutrition.toString());
        } catch (SQLException e) {
            log.error(e.getMessage());
            return false;
        }
        return result;
    }

    @Override
    public boolean delete(long id) {
        boolean result;
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement st = conn.prepareStatement(
                     "DELETE FROM NUTRITION WHERE ID = ?")) {
            st.setLong(1, id);
            int rows = st.executeUpdate();
            result = rows != 0;
            log.info("Deleted: " + result);
        } catch (SQLException e) {
            log.error(e.getMessage());
            return false;
        }
        return result;
    }
}
