package health.repository.impl;

import health.model.ImbHistory;
import health.repository.ImbHistoryRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ImbHistoryRepositoryImpl implements ImbHistoryRepository {
    private static final Logger log = LogManager.getLogger(ImbHistoryRepositoryImpl.class);

    private static final String URL = "jdbc:h2:~/health";
    private static final String USER = "sa";
    private static final String PASSWORD = "";

    @Override
    public boolean add(ImbHistory imbHistory) {
        boolean result;
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement st = conn.prepareStatement(
                     "INSERT INTO IMB_HISTORY (DATE, USER_ID, VALUE) VALUES (?, ?, ?);")) {
            st.setDate(1, new java.sql.Date(imbHistory.getDate().getTime()));
            st.setLong(2, imbHistory.getUserId());
            st.setDouble(3, imbHistory.getValue());
            int rows = st.executeUpdate();
            result = rows != 0;
            log.info("Persisted: " + result + " imb: " + imbHistory.toString());
        } catch (SQLException e) {
            log.error(e.getMessage());
            return false;
        }
        return result;
    }

    @Override
    public boolean delete(ImbHistory imbHistory) {

        return false;
    }

    @Override
    public List<ImbHistory> getByUsername(String username) {
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement st = conn.prepareStatement(
                     "SELECT * FROM IMB_HISTORY WHERE USER_ID = (SELECT ID FROM USERS WHERE LOGIN = ?);")) {
            st.setString(1, username);
            List<ImbHistory> imbHistory = new ArrayList<>();
            ImbHistory imb;
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    imb = new ImbHistory();
                    imb.setId(rs.getLong("id"));
                    imb.setDate(rs.getDate("date"));
                    imb.setUserId(rs.getLong("user_id"));
                    imb.setValue(rs.getDouble("value"));

                    imbHistory.add(imb);
                }
            } catch (SQLException e) {
                log.error(e.getMessage());
            }
            return imbHistory;
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return new ArrayList<>();
    }

}
