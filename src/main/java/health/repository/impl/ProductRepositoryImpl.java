package health.repository.impl;

import health.model.Product;
import health.repository.ProductRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class ProductRepositoryImpl implements ProductRepository {

    private static final Logger log = LogManager.getLogger(ProductRepositoryImpl.class);

    private static final String URL = "jdbc:h2:~/health";
    private static final String USER = "sa";
    private static final String PASSWORD = "";

    @Override
    public Optional<Product> getById(long id) {
        Product product = null;
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement st = conn.prepareStatement(
                     "SELECT * FROM PRODUCTS WHERE ID = ?")) {
            st.setLong(1, id);
            product = getProduct(st).get();
            log.info("Found product by id: " + product);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return Optional.ofNullable(product);
    }

    @Override
    public Optional<Product> getByName(String name) {
        Product product = null;
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement st = conn.prepareStatement(
                     "SELECT * FROM PRODUCTS WHERE NAME = ?")) {
            st.setString(1, name);
            product = getProduct(st).get();
            log.info("Found product by name: " + product);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return Optional.ofNullable(product);
    }

    private Optional<Product> getProduct(PreparedStatement preparedStatement) {
        Product product = null;
        try (ResultSet rs = preparedStatement.executeQuery()) {
            while (rs.next()) {
                product = new Product();
                product.setId(rs.getLong("id"));
                product.setName(rs.getString("name"));
                product.setType(rs.getString("type"));

                product.setProtein(rs.getDouble("protein"));
                product.setFat(rs.getDouble("fat"));
                product.setCarbohydrate(rs.getDouble("carbohydrate"));
                product.setKkal(rs.getDouble("kkal"));
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return Optional.ofNullable(product);
    }

    @Override
    public List<Product> getByType(String type) {
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement st = conn.prepareStatement(
                     "SELECT * FROM PRODUCTS WHERE TYPE = ?")) {
            st.setString(1, type);
            List<Product> products = new ArrayList<>();
            Product product;
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    product = new Product();
                    product.setId(rs.getLong("id"));
                    product.setName(rs.getString("name"));
                    product.setProtein(rs.getDouble("protein"));
                    product.setFat(rs.getDouble("fat"));
                    product.setCarbohydrate(rs.getDouble("carbohydrate"));
                    product.setKkal(rs.getDouble("kkal"));
                    product.setType(rs.getString("type"));

                    products.add(product);
                }
            } catch (SQLException e) {
                log.error(e.getMessage());
            }
            return products;
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return new ArrayList<>();
    }
}
