package health.repository;

import health.model.User;

import java.util.Optional;

public interface UserRepository {

    Optional<User> getByName(String username);

    boolean add(User user);

    boolean delete(User user);

    Optional<User> getById(Long userId);

    boolean update(User user, User newUser);

}
