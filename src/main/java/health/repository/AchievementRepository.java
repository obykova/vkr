package health.repository;

import health.model.Achievement;

import java.util.Optional;

public interface AchievementRepository {

    Optional<Achievement> getById(long id);

    Optional<Achievement> getByName(String name);
}
