package health.model;

public class Category {
    private long id;
    private String value;

    public Category() {
    }

    public Category(long id, String value) {
        this.id = id;
        this.value = value;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", value='" + value + '\'' +
                '}';
    }
}
