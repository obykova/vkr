package health.model;

import java.util.Date;

public class User {

    private long id;
    private String login;
    private String password;
    private String question;
    private String word;
    private String name;
    private double height;
    private double weight;
    private Date birthday;
    private String gender;
    private Date dateRegister;
    private String role;

    private static final Long DEFAULT_ID = -1L;
    private static final String EMPTY_STRING = "";
    private static final double DEFAULT_DOUBLE = -1;
    private static final Date DEFAULT_DATE = new Date();

    public User() {
        this.id = DEFAULT_ID;
        this.login = EMPTY_STRING;
        this.password = EMPTY_STRING;
        this.question = EMPTY_STRING;
        this.word = EMPTY_STRING;
        this.name = EMPTY_STRING;
        this.height = DEFAULT_DOUBLE;
        this.weight = DEFAULT_DOUBLE;
        this.birthday = DEFAULT_DATE;
        this.gender = EMPTY_STRING;
        this.dateRegister = DEFAULT_DATE;
        this.role = EMPTY_STRING;
    }

    public User(long id, String login, String password,
                String question, String word, String name, double height,
                double weight, Date birthday, String gender, Date dateRegister, String role) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.question = question;
        this.word = word;
        this.name = name;
        this.height = height;
        this.weight = weight;
        this.birthday = birthday;
        this.gender = gender;
        this.dateRegister = dateRegister;
        this.role = role;
    }

    public User(String login, String password,
                String question, String word,
                String name, double height, double weight,
                Date birthday, String gender, Date dateRegister, String role) {
        this.login = login;
        this.password = password;
        this.question = question;
        this.word = word;
        this.name = name;
        this.height = height;
        this.weight = weight;
        this.birthday = birthday;
        this.gender = gender;
        this.dateRegister = dateRegister;
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public String getFormatGender() {
        if (gender.equals("M")) return "Мужской";
        return "Женский";
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(Date dateRegister) {
        this.dateRegister = dateRegister;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", question='" + question + '\'' +
                ", word='" + word + '\'' +
                ", name='" + name + '\'' +
                ", height=" + height +
                ", weight=" + weight +
                ", birthday=" + birthday +
                ", gender='" + gender + '\'' +
                ", dateRegister=" + dateRegister +
                ", role='" + role + '\'' +
                '}';
    }
}
