package health.model;

public class Achievement {

    private long id;
    private String name;
    private String description;
    private String image;
    private int condition;
    private String conditionCategory;

    public Achievement() {
    }

    public Achievement(long id, String name, String description,
                       String image, int condition, String conditionCategory) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
        this.condition = condition;
        this.conditionCategory = conditionCategory;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getCondition() {
        return condition;
    }

    public void setCondition(int condition) {
        this.condition = condition;
    }

    public String getConditionCategory() {
        return conditionCategory;
    }

    public void setConditionCategory(String conditionCategory) {
        this.conditionCategory = conditionCategory;
    }

    @Override
    public String toString() {
        return "Achievement{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                ", condition=" + condition +
                ", conditionCategory='" + conditionCategory + '\'' +
                '}';
    }
}
