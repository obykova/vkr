package health.model;

import java.util.Date;

public class Nutrition {
    private long id;
    private long userId;
    private long productId;
    private Date date;
    private String time;
    private double massa;

    public Nutrition() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getMassa() {
        return massa;
    }

    public void setMassa(double massa) {
        this.massa = massa;
    }

    @Override
    public String toString() {
        return "Nutrition{" +
                "id=" + id +
                ", userId=" + userId +
                ", productId=" + productId +
                ", date=" + date +
                ", time='" + time + '\'' +
                ", massa=" + massa +
                '}';
    }
}
