package health.model;

public class Norm {
    private long id;
    private long categoryId;
    private long userId;
    private double value;

    public Norm() {
    }

    public Norm(long id, long categoryId, long userId, double value) {
        this.id = id;
        this.categoryId = categoryId;
        this.userId = userId;
        this.value = value;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Norm{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", userId=" + userId +
                ", value=" + value +
                '}';
    }
}
