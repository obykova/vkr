package health.model;

public class Water {

    private long id;
    private double value;
    private long userId;
    private long dateId;

    private static final Long DEFAULT_ID = -1L;
    private static final double DEFAULT_DOUBLE = -1;

    public Water() {
        this.id = DEFAULT_ID;
        this.value = DEFAULT_DOUBLE;
        this.userId = DEFAULT_ID;
        this.dateId = DEFAULT_ID;
    }

    public long getId() {
        return id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getDateId() {
        return dateId;
    }

    public void setDateId(long dateId) {
        this.dateId = dateId;
    }

    @Override
    public String toString() {
        return "Water{" +
                "id=" + id +
                ", value=" + value +
                ", userId=" + userId +
                ", dateId=" + dateId +
                '}';
    }
}
