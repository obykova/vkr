package health.model;

import java.util.Date;

public class Sleep {
    private long id;
    private long userId;
    //TODO тип времени
    private Date timeBegin;
    private Date timeEnd;
    private long dateId;

    public Sleep() {
    }

    public Sleep(long id, long userId, Date timeBegin, Date timeEnd, long dateId) {
        this.id = id;
        this.userId = userId;
        this.timeBegin = timeBegin;
        this.timeEnd = timeEnd;
        this.dateId = dateId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Date getTimeBegin() {
        return timeBegin;
    }

    public void setTimeBegin(Date timeBegin) {
        this.timeBegin = timeBegin;
    }

    public Date getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(Date timeEnd) {
        this.timeEnd = timeEnd;
    }

    public long getDateId() {
        return dateId;
    }

    public void setDateId(long dateId) {
        this.dateId = dateId;
    }

    @Override
    public String toString() {
        return "Sleep{" +
                "id=" + id +
                ", userId=" + userId +
                ", timeBegin=" + timeBegin +
                ", timeEnd=" + timeEnd +
                ", dateId=" + dateId +
                '}';
    }
}
