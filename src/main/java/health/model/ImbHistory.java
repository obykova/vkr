package health.model;

import java.util.Date;

public class ImbHistory {

    private long id;
    private Date date;
    private long userId;
    private double value;

    public ImbHistory() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ImbHistory{" +
                "id=" + id +
                ", date=" + date +
                ", userId=" + userId +
                ", value=" + value +
                '}';
    }
}
