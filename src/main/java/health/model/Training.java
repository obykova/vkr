package health.model;

public class Training {
    private long id;
    private String name;
    private double kkal;

    public Training() {
    }

    public Training(long id, String name, double kkal) {
        this.id = id;
        this.name = name;
        this.kkal = kkal;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getKkal() {
        return kkal;
    }

    public void setKkal(double kkal) {
        this.kkal = kkal;
    }

    @Override
    public String toString() {
        return "Training{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", kkal=" + kkal +
                '}';
    }
}
