package health.model;

public class Product {

    private long id;
    private String name;
    private double protein;
    private double fat;
    private double carbohydrate;
    private double kkal;
    private String type;

    private static final Long DEFAULT_ID = -1L;
    private static final double DEFAULT_DOUBLE = -1;
    private static final String EMPTY_STRING = "";

    public Product() {
        this.id = DEFAULT_ID;
        this.name = EMPTY_STRING;
        this.protein = DEFAULT_DOUBLE;
        this.fat = DEFAULT_DOUBLE;
        this.carbohydrate = DEFAULT_DOUBLE;
        this.kkal = DEFAULT_DOUBLE;
        this.type = EMPTY_STRING;
    }

    public Product(String name, double protein, double fat, double carbohydrate, double kkal, String type) {
        this.name = name;
        this.protein = protein;
        this.fat = fat;
        this.carbohydrate = carbohydrate;
        this.kkal = kkal;
        this.type = type;
    }

    public Product(long id, String name, double protein, double fat, double carbohydrate, double kkal, String type) {
        this.id = id;
        this.name = name;
        this.protein = protein;
        this.fat = fat;
        this.carbohydrate = carbohydrate;
        this.kkal = kkal;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getProtein() {
        return protein;
    }

    public void setProtein(double protein) {
        this.protein = protein;
    }

    public double getFat() {
        return fat;
    }

    public void setFat(double fat) {
        this.fat = fat;
    }

    public double getCarbohydrate() {
        return carbohydrate;
    }

    public void setCarbohydrate(double carbohydrate) {
        this.carbohydrate = carbohydrate;
    }

    public double getKkal() {
        return kkal;
    }

    public void setKkal(double kkal) {
        this.kkal = kkal;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", protein=" + protein +
                ", fat=" + fat +
                ", carbohydrate=" + carbohydrate +
                ", kkal=" + kkal +
                ", type='" + type + '\'' +
                '}';
    }
}
