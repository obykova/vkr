package health.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.core.type.filter.RegexPatternTypeFilter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.regex.Pattern;

@Configuration
@Import({DataSourceConfig.class, SecurityConfig.class})
@ComponentScan(basePackages = {"health.config", "health.repository", "health.service", "health.util"},
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ANNOTATION, value = EnableWebMvc.class),
                @ComponentScan.Filter(type = FilterType.CUSTOM, value = RootConfig.ControllerPackage.class)
        }
)

public class RootConfig {
    public static class ControllerPackage extends RegexPatternTypeFilter {
        public ControllerPackage() {
            super(Pattern.compile(""));
        }
    }
}
