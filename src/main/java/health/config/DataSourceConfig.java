package health.config;

import health.util.PropertyReader;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class DataSourceConfig {

    private PropertyReader propertyReader;

    private static final String SHOW_SQL = "hibernate.show_sql";
    private static final String FORMAT_SQL = "hibernate.format_sql";
    private static final String HBM2DDL = "hibernate.hbm2ddl.auto";
    private static final String DIALECT = "dialect";
    private static final String DRIVER = "db.driver";
    private static final String URL = "db.url";
    private static final String USERNAME = "db.username";
    private static final String PACKAGES = "db.packages";

    @Value("${dialect}")
    private String dialect;

    public DataSourceConfig(PropertyReader propertyReader) {
        this.propertyReader = propertyReader;
    }

    @Bean
    public DataSource dataSource() {
        Properties properties = propertyReader.getProperties();

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(properties.getProperty(DRIVER));
        dataSource.setUrl(properties.getProperty(URL));
        dataSource.setUsername(properties.getProperty(USERNAME));

        return dataSource;
    }

    @Bean
    public LocalSessionFactoryBean sessionFactoryBean(DataSource dataSource) {
        Properties properties = propertyReader.getProperties();
        LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
        factoryBean.setDataSource(dataSource);

        Properties prop = new Properties();
        prop.setProperty(SHOW_SQL, properties.getProperty(SHOW_SQL));
        prop.setProperty(FORMAT_SQL, properties.getProperty(FORMAT_SQL));
        prop.setProperty(HBM2DDL, properties.getProperty(HBM2DDL));
        prop.setProperty(DIALECT, properties.getProperty(DIALECT));

        factoryBean.setHibernateProperties(prop);
        factoryBean.setPackagesToScan(properties.getProperty(PACKAGES));
        return factoryBean;
    }

    @Bean
    public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory);
        return transactionManager;
    }
}
