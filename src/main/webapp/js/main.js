(function($) {
    /*---------------------------------------------------- */
    /* Preloader
    ------------------------------------------------------ */
    $(window).load(function() {
        $("#loader").fadeOut("slow", function(){
            $("#preloader").delay(300).fadeOut("slow");
        });
        $("#sign-form").addClass('hddn');
    })

    /*---------------------------------------------------- */
    /* Slow Scroll
    ------------------------------------------------------ */
    var $page = $('html, body');
    $('a[href*="#"]').click(function() {
        $page.animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 1000);
        return false;
    });

    /*---------------------------------------------------- */
    /* Login animation
    ------------------------------------------------------ */

    $('.lblInp input').keyup(function () {
        if ($(this).val().length > 0) {
            $(this).closest('.lblInp input').addClass('filled');
        } else {
            $(this).closest('.lblInp input').removeClass('filled');
        }
    });
    $('.lblInp select').change(function () {
        $(this).closest('.lblInp select').addClass('filled');
    });
    $('.lblInp #birthDate').change(function () {
            $(this).closest('.lblInp #birthDate').addClass('filled');
    });


    /*---------------------------------------------------- */
    /* Show password animation
    ------------------------------------------------------ */

    $('body').on('click', '.password-checkbox', function(){
        if ($(this).is(':checked')){
            $('#password').attr('type', 'text');
        } else {
            $('#password').attr('type', 'password');
        }
    });
})(jQuery);

/*---------------------------------------------------- */
/* Logo Animation
------------------------------------------------------ */
const spans = document.querySelectorAll('.word span');
spans.forEach((span, idx) => {
    span.addEventListener('click', (e) => {
        e.target.classList.add('active');
    });
    span.addEventListener('animationend', (e) => {
        e.target.classList.remove('active');
    });

    // Initial animation
    setTimeout(() => {
        span.classList.add('active');
    }, 750 * (idx+1))
});

/*---------------------------------------------------- */
/* Reset labels (imt calculator)
------------------------------------------------------ */
document.getElementById("clearButton").onclick = function(e) {
    document.getElementById("height").value = "";
    document.getElementById("weight").value = "";
}
